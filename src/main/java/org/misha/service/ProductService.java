package org.misha.service;

import org.apache.commons.lang3.time.StopWatch;
import org.misha.domain.Product;
import org.misha.domain.ProductInfo;
import org.misha.domain.Review;
import org.slf4j.Logger;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import static org.misha.Util.stopWatch;

public class ProductService {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(ProductService.class);
    private final ProductInfoService productInfoService;
    private final ProductReviewService productReviewService;

    public ProductService(ProductInfoService productInfoService, ProductReviewService productReviewService) {
        this.productInfoService = productInfoService;
        this.productReviewService = productReviewService;
    }

    public Product retrieveProductDetails(String productId) {
        stopWatch.start();
        CountDownLatch latch = new CountDownLatch(2);
        AtomicReference<ProductInfo> productInfo = new AtomicReference<>();
        AtomicReference<Review> review = new AtomicReference<>();
        Thread thread = new Thread(() -> {
            productInfo.set(productInfoService.retrieveProductInfo("123ABC")); latch.countDown();
        });
        Thread thread1 = new Thread(() -> {
            review.set(productReviewService.retrieveReviews("123ABC")); latch.countDown();
        });
        thread1.start();
        thread.start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        Product product = new Product(productId, productInfo.get(), review.get());
        log.info("Product details : {}", product);
        stopWatch.stop();
        log.info("Time taken to retrieve product details : {}", stopWatch.getTime());
        return product;
    }

    private Product getProductSeq(String productId) {
        stopWatch.reset();
        stopWatch.start();
        ProductInfo productInfo = productInfoService.retrieveProductInfo(productId);
        Review review = productReviewService.retrieveReviews(productId);
        Product product = new Product(productId, productInfo, review);
        stopWatch.stop();
        log.info("Time taken to retrieve product details seq: {}",
                stopWatch.getTime());
        log.info("Product details : {}", product);
        return product;
    }

    public static void main(String[] args) {
        ProductService productService = new ProductService(new ProductInfoService(), new ProductReviewService());
        productService.retrieveProductDetails("123ABC");
        productService.getProductSeq("123ABC");
    }
}
