package org.misha.domain;

public class ProductOption {
    private Integer productionOptionId;
    private String size;
    private String color;
    private double price;

    public ProductOption(Integer productionOptionId, String size, String color, double price) {
        this.productionOptionId = productionOptionId;
        this.size = size;
        this.color = color;
        this.price = price;
    }

    public ProductOption() {}

    public static ProductOptionBuilder builder() {return new ProductOptionBuilder();}

    public Integer getProductionOptionId() {return this.productionOptionId;}

    public String getSize() {return this.size;}

    public String getColor() {return this.color;}

    public double getPrice() {return this.price;}

    public void setProductionOptionId(Integer productionOptionId) {this.productionOptionId = productionOptionId;}

    public void setSize(String size) {this.size = size;}

    public void setColor(String color) {this.color = color;}

    public void setPrice(double price) {this.price = price;}

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof ProductOption)) return false;
        final ProductOption other = (ProductOption) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$productionOptionId = this.getProductionOptionId();
        final Object other$productionOptionId = other.getProductionOptionId();
        if (this$productionOptionId == null ? other$productionOptionId != null : !this$productionOptionId.equals(other$productionOptionId))
            return false;
        final Object this$size = this.getSize();
        final Object other$size = other.getSize();
        if (this$size == null ? other$size != null : !this$size.equals(other$size)) return false;
        final Object this$color = this.getColor();
        final Object other$color = other.getColor();
        if (this$color == null ? other$color != null : !this$color.equals(other$color)) return false;
        if (Double.compare(this.getPrice(), other.getPrice()) != 0) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {return other instanceof ProductOption;}

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $productionOptionId = this.getProductionOptionId();
        result = result * PRIME + ($productionOptionId == null ? 43 : $productionOptionId.hashCode());
        final Object $size = this.getSize();
        result = result * PRIME + ($size == null ? 43 : $size.hashCode());
        final Object $color = this.getColor();
        result = result * PRIME + ($color == null ? 43 : $color.hashCode());
        final long $price = Double.doubleToLongBits(this.getPrice());
        result = result * PRIME + (int) ($price >>> 32 ^ $price);
        return result;
    }

    public String toString() {return "ProductOption(productionOptionId=" + this.getProductionOptionId() + ", size=" + this.getSize() + ", color=" + this.getColor() + ", price=" + this.getPrice() + ")";}

    public static class ProductOptionBuilder {
        private Integer productionOptionId;
        private String size;
        private String color;
        private double price;

        ProductOptionBuilder() {}

        public ProductOptionBuilder productionOptionId(Integer productionOptionId) {
            this.productionOptionId = productionOptionId;
            return this;
        }

        public ProductOptionBuilder size(String size) {
            this.size = size;
            return this;
        }

        public ProductOptionBuilder color(String color) {
            this.color = color;
            return this;
        }

        public ProductOptionBuilder price(double price) {
            this.price = price;
            return this;
        }

        public ProductOption build() {
            return new ProductOption(this.productionOptionId, this.size, this.color, this.price);
        }

        public String toString() {return "ProductOption.ProductOptionBuilder(productionOptionId=" + this.productionOptionId + ", size=" + this.size + ", color=" + this.color + ", price=" + this.price + ")";}
    }
}
