package org.misha.domain;

public class Review {
    private int noOfReviews;
    private double overallRating;

    public Review(int noOfReviews, double overallRating) {
        this.noOfReviews = noOfReviews;
        this.overallRating = overallRating;
    }

    public Review() {}

    public int getNoOfReviews() {return this.noOfReviews;}

    public double getOverallRating() {return this.overallRating;}

    public void setNoOfReviews(int noOfReviews) {this.noOfReviews = noOfReviews;}

    public void setOverallRating(double overallRating) {this.overallRating = overallRating;}

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Review)) return false;
        final Review other = (Review) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getNoOfReviews() != other.getNoOfReviews()) return false;
        if (Double.compare(this.getOverallRating(), other.getOverallRating()) != 0) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {return other instanceof Review;}

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + this.getNoOfReviews();
        final long $overallRating = Double.doubleToLongBits(this.getOverallRating());
        result = result * PRIME + (int) ($overallRating >>> 32 ^ $overallRating);
        return result;
    }

    public String toString() {return "Review(noOfReviews=" + this.getNoOfReviews() + ", overallRating=" + this.getOverallRating() + ")";}
}
