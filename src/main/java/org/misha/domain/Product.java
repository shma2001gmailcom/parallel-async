package org.misha.domain;

import lombok.NonNull;

public class Product {
    @NonNull
    private String productId;
    @NonNull
    private ProductInfo productInfo;
    @NonNull
    private Review review;
    private Inventory inventory;

    public Product(@NonNull String productId, @NonNull ProductInfo productInfo, @NonNull Review review) {
        this.productId = productId;
        this.productInfo = productInfo;
        this.review = review;
    }

    public Product(@NonNull String productId, @NonNull ProductInfo productInfo, @NonNull Review review, Inventory inventory) {
        this.productId = productId;
        this.productInfo = productInfo;
        this.review = review;
        this.inventory = inventory;
    }

    public Product() {}

    public static ProductBuilder builder() {return new ProductBuilder();}

    public @NonNull String getProductId() {return this.productId;}

    public @NonNull ProductInfo getProductInfo() {return this.productInfo;}

    public @NonNull Review getReview() {return this.review;}

    public Inventory getInventory() {return this.inventory;}

    public void setProductId(@NonNull String productId) {this.productId = productId;}

    public void setProductInfo(@NonNull ProductInfo productInfo) {this.productInfo = productInfo;}

    public void setReview(@NonNull Review review) {this.review = review;}

    public void setInventory(Inventory inventory) {this.inventory = inventory;}

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof Product)) return false;
        final Product other = (Product) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$productId = this.getProductId();
        final Object other$productId = other.getProductId();
        if (this$productId == null ? other$productId != null : !this$productId.equals(other$productId)) return false;
        final Object this$productInfo = this.getProductInfo();
        final Object other$productInfo = other.getProductInfo();
        if (this$productInfo == null ? other$productInfo != null : !this$productInfo.equals(other$productInfo))
            return false;
        final Object this$review = this.getReview();
        final Object other$review = other.getReview();
        if (this$review == null ? other$review != null : !this$review.equals(other$review)) return false;
        final Object this$inventory = this.getInventory();
        final Object other$inventory = other.getInventory();
        if (this$inventory == null ? other$inventory != null : !this$inventory.equals(other$inventory)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {return other instanceof Product;}

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $productId = this.getProductId();
        result = result * PRIME + ($productId == null ? 43 : $productId.hashCode());
        final Object $productInfo = this.getProductInfo();
        result = result * PRIME + ($productInfo == null ? 43 : $productInfo.hashCode());
        final Object $review = this.getReview();
        result = result * PRIME + ($review == null ? 43 : $review.hashCode());
        final Object $inventory = this.getInventory();
        result = result * PRIME + ($inventory == null ? 43 : $inventory.hashCode());
        return result;
    }

    public String toString() {return "Product(productId=" + this.getProductId() + ", productInfo=" + this.getProductInfo() + ", review=" + this.getReview() + ", inventory=" + this.getInventory() + ")";}

    public static class ProductBuilder {
        private @NonNull String productId;
        private @NonNull ProductInfo productInfo;
        private @NonNull Review review;
        private Inventory inventory;

        ProductBuilder() {}

        public ProductBuilder productId(@NonNull String productId) {
            this.productId = productId;
            return this;
        }

        public ProductBuilder productInfo(@NonNull ProductInfo productInfo) {
            this.productInfo = productInfo;
            return this;
        }

        public ProductBuilder review(@NonNull Review review) {
            this.review = review;
            return this;
        }

        public ProductBuilder inventory(Inventory inventory) {
            this.inventory = inventory;
            return this;
        }

        public Product build() {
            return new Product(this.productId, this.productInfo, this.review, this.inventory);
        }

        public String toString() {return "Product.ProductBuilder(productId=" + this.productId + ", productInfo=" + this.productInfo + ", review=" + this.review + ", inventory=" + this.inventory + ")";}
    }
}
