package org.misha.domain;

import java.util.List;

public class ProductInfo {
    private String productId;
    private List<ProductOption> productOptions;

    public ProductInfo(String productId, List<ProductOption> productOptions) {
        this.productId = productId;
        this.productOptions = productOptions;
    }

    public ProductInfo() {}

    public static ProductInfoBuilder builder() {return new ProductInfoBuilder();}

    public String getProductId() {return this.productId;}

    public List<ProductOption> getProductOptions() {return this.productOptions;}

    public void setProductId(String productId) {this.productId = productId;}

    public void setProductOptions(List<ProductOption> productOptions) {this.productOptions = productOptions;}

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof ProductInfo)) return false;
        final ProductInfo other = (ProductInfo) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$productId = this.getProductId();
        final Object other$productId = other.getProductId();
        if (this$productId == null ? other$productId != null : !this$productId.equals(other$productId)) return false;
        final Object this$productOptions = this.getProductOptions();
        final Object other$productOptions = other.getProductOptions();
        if (this$productOptions == null ? other$productOptions != null : !this$productOptions.equals(other$productOptions))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {return other instanceof ProductInfo;}

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $productId = this.getProductId();
        result = result * PRIME + ($productId == null ? 43 : $productId.hashCode());
        final Object $productOptions = this.getProductOptions();
        result = result * PRIME + ($productOptions == null ? 43 : $productOptions.hashCode());
        return result;
    }

    public String toString() {return "ProductInfo(productId=" + this.getProductId() + ", productOptions=" + this.getProductOptions() + ")";}

    public static class ProductInfoBuilder {
        private String productId;
        private List<ProductOption> productOptions;

        ProductInfoBuilder() {}

        public ProductInfoBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }

        public ProductInfoBuilder productOptions(List<ProductOption> productOptions) {
            this.productOptions = productOptions;
            return this;
        }

        public ProductInfo build() {
            return new ProductInfo(this.productId, this.productOptions);
        }

        public String toString() {return "ProductInfo.ProductInfoBuilder(productId=" + this.productId + ", productOptions=" + this.productOptions + ")";}
    }
}