package org.misha;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;

import static java.lang.Thread.sleep;

public class Util {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(Util.class);
    public static StopWatch stopWatch = new StopWatch();

    public static void delay(long delayMilliSeconds) {
        try {
            sleep(delayMilliSeconds);
        } catch (Exception e) {
            log.info("Exception is :" + e.getMessage());
        }
    }
}
